package training.kotlin.springboot.utils

import training.kotlin.springboot.model.Charge
import training.kotlin.springboot.vo.ChargeVO

class ChargeVOUtils {
    companion object {
        fun convertChargeToChargeVO(charge: Charge): ChargeVO {
            var chargeVO = ChargeVO()

            chargeVO.chargeId = charge.chargeId
            chargeVO.cardNo = charge.cardNo
            chargeVO.brand = charge.brand
            chargeVO.amount = charge.amount
            chargeVO.status = charge.status

            return chargeVO
        }
    }
}

