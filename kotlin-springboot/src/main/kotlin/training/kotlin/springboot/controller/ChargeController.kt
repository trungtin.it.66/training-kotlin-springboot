package training.kotlin.springboot.controller

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.PathVariable
import training.kotlin.springboot.event.ChargeEvent
import training.kotlin.springboot.event.ChargeEventAction
import training.kotlin.springboot.event.ChargeEventProducer
import training.kotlin.springboot.model.Charge
import training.kotlin.springboot.repository.ChargeRepository
import training.kotlin.springboot.response.ErrorResponseObject
import training.kotlin.springboot.response.ResponseObject
import training.kotlin.springboot.response.ResponseStatus
import training.kotlin.springboot.response.SuccessResponseObject
import training.kotlin.springboot.utils.ChargeVOUtils
import training.kotlin.springboot.vo.ChargeVO
import java.util.stream.Collector
import java.util.stream.Collectors
import kotlin.streams.asStream

@RestController
@RequestMapping("/api/charges")
class ChargeController {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    lateinit var chargeRepository: ChargeRepository

    @Autowired
    lateinit var chargeEventProducer: ChargeEventProducer

    @GetMapping("")
    fun getAllCharges(): ResponseObject {
        logger.info("get all charges")
        try {
            var searchResult = chargeRepository.findAll()
            var chargeVOs = searchResult.asSequence().asStream().map { charge -> ChargeVOUtils.convertChargeToChargeVO(charge) }.collect(Collectors.toList())
            return buildSuccessResponse(chargeVOs, "All entities have been retrieved successfully!")
        } catch (e: Exception) {
            logger.error("Failed to retrieve all entities!", e)
            return buildErrorResponse("Failed to retrieve all entities!")
        }
    }

    @GetMapping("/{chargeId}")
    fun getChargeByChargeId(@PathVariable("chargeId") chargeId: Int): ResponseObject {
        logger.info("get charge by chargeId")
        var chargeOptional = chargeRepository.findByChargeId(chargeId)

        if (chargeOptional.isPresent) {
            return buildSuccessResponse(ChargeVOUtils.convertChargeToChargeVO(chargeOptional.get()), "The entity has been retrieved successfully!")
        } else {
            return buildErrorResponse("The entity doesn't exist!")
        }
    }

    @PostMapping
    fun createCharge(@RequestBody chargeVO: ChargeVO): ResponseObject {
        logger.info("create charge")
        var charge = chargeVO.toCharge()

        try {
            chargeRepository.save(charge)
            chargeVO.chargeId = charge.chargeId

            chargeEventProducer.send(ChargeEvent(ChargeEventAction.CREATE, System.currentTimeMillis(), charge))

            return buildSuccessResponse(chargeVO, "The entity has been created successfully!")
        } catch (e: Exception) {
            logger.error("Failed to create the entity!", e)
            return buildErrorResponse("Failed to create the entity!")
        }
    }

    @PutMapping("/{chargeId}")
    fun updateCharge(@RequestBody chargeVO: ChargeVO, @PathVariable("chargeId") chargeId: Int): ResponseObject {
        logger.info("update charge")

        var chargeOptional = chargeRepository.findByChargeId(chargeId)

        if (chargeOptional.isEmpty) {
            return buildErrorResponse("The entity doesn't exist!")
        }

        var charge = chargeOptional.get()
        charge.cardNo = chargeVO.cardNo
        charge.amount = chargeVO.amount
        charge.status = chargeVO.status
        charge.brand = chargeVO.brand

        try {
            chargeRepository.save(charge)

            chargeEventProducer.send(ChargeEvent(ChargeEventAction.UPDATE, System.currentTimeMillis(), charge))

            return buildSuccessResponse(chargeVO, "The entity has been updated successfully!")
        } catch (e: Exception) {
            logger.error("Failed to update the entity!", e)
            return buildErrorResponse("Failed to update the entity!")
        }
    }

    @DeleteMapping("/{chargeId}")
    fun deleteCharge(@PathVariable("chargeId") chargeId: Int): ResponseObject {
        logger.info("delete charge")
        try {
            var chargeOptional = chargeRepository.findByChargeId(chargeId)

            if (chargeOptional.isPresent) {
                chargeRepository.delete(chargeOptional.get())

                chargeEventProducer.send(ChargeEvent(ChargeEventAction.DELETE, System.currentTimeMillis(), chargeOptional.get()))

            }

            return buildSuccessResponse(null, "The entity has been deleted successfully!")
        } catch (e: Exception) {
            logger.error("Failed to delete the entity!", e)
            return buildErrorResponse("Failed to delete the entity!")
        }
    }

    private fun buildErrorResponse(errorMessage: String): ResponseObject {
        var responseObject = ErrorResponseObject()

        responseObject.status = ResponseStatus.ERROR
        responseObject.message = errorMessage

        return responseObject
    }

    private fun <T> buildSuccessResponse(data: T, message: String): ResponseObject {
        var responseObject = SuccessResponseObject<T>()

        responseObject.status = ResponseStatus.SUCCESS
        responseObject.data = data
        responseObject.message = message

        return responseObject

    }
}