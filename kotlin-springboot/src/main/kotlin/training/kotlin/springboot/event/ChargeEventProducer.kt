package training.kotlin.springboot.event

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord
import org.apache.avro.generic.GenericRecordBuilder
import org.slf4j.LoggerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import training.kotlin.springboot.model.Charge
import java.io.File

@Component
class ChargeEventProducer(private val kafkaTemplate: KafkaTemplate<String, GenericRecord>) {

    var logger = LoggerFactory.getLogger(javaClass)

    val schema = Schema.Parser().parse(File("src/main/resources/charge_event.avsc"))

    fun send(chargeEvent: ChargeEvent) {
        kafkaTemplate.send("charge_events", buildRecord(chargeEvent))
    }

    fun buildRecord(chargeEvent: ChargeEvent): GenericRecord {
        var avroChargeObject = GenericRecordBuilder(schema.getField(ChargeEvent.ATTR_CHARGE_OBJECT).schema()).apply {
            set(Charge.ATTR_CHARGE_ID, chargeEvent.chargeObject.chargeId)
            set(Charge.ATTR_CARD_NO, chargeEvent.chargeObject.cardNo)
            set(Charge.ATTR_AMOUNT, chargeEvent.chargeObject.amount)
            set(Charge.ATTR_BRAND, chargeEvent.chargeObject.brand)
            set(Charge.ATTR_STATUS, chargeEvent.chargeObject.status)
        }.build()
        return GenericRecordBuilder(schema).apply {
            set(ChargeEvent.ATTR_ACTION, chargeEvent.action.toString())
            set(ChargeEvent.ATTR_TIME, chargeEvent.time)
            set(ChargeEvent.ATTR_CHARGE_OBJECT, avroChargeObject)
        }.build()
    }

}