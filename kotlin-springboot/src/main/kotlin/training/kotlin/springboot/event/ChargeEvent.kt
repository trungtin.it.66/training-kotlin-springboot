package training.kotlin.springboot.event

import training.kotlin.springboot.model.Charge

class ChargeEvent (
    var action: ChargeEventAction,
    var time: Long = 0,
    var chargeObject: Charge
) {
    companion object {
        val ATTR_ACTION = "action"
        val ATTR_TIME = "time"
        val ATTR_CHARGE_OBJECT = "chargeObject"
    }
}