package training.kotlin.springboot.event

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.apache.avro.generic.GenericRecord
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component
import training.kotlin.springboot.model.Charge
import training.kotlin.springboot.model.ChargeHistory
import training.kotlin.springboot.repository.ChargeHistoryRepository
import java.math.BigInteger
import java.sql.Timestamp
import java.time.LocalDateTime

@Component
class ChargeEventConsumer {

    private val logger = LoggerFactory.getLogger(javaClass)

    var jsonMapper = ObjectMapper().registerModule(KotlinModule())

    @Autowired
    lateinit var chargeHistoryRepository: ChargeHistoryRepository

    @KafkaListener(topics = ["charge_events"], groupId = "group_1")
    fun processMessage(avroChargeEvent: GenericRecord) {
        var chargeEventContent = jsonMapper.writeValueAsString(buildChargeEvent(avroChargeEvent))

        logger.info("Received chargeEvent '{}'", chargeEventContent)

        var chargeHistory = ChargeHistory(BigInteger.valueOf(0), chargeEventContent, Timestamp.valueOf(LocalDateTime.now()))
        chargeHistoryRepository.save(chargeHistory)
    }

    fun buildChargeEvent(avroChargeEvent: GenericRecord): ChargeEvent {
        var avroCharge = avroChargeEvent[ChargeEvent.ATTR_CHARGE_OBJECT] as GenericRecord
        var chargeEvent = ChargeEvent(
                ChargeEventAction.valueOf(avroChargeEvent[ChargeEvent.ATTR_ACTION].toString()),
                avroChargeEvent[ChargeEvent.ATTR_TIME] as Long,
                Charge(
                        avroCharge[Charge.ATTR_CHARGE_ID] as Int,
                        avroCharge[Charge.ATTR_CARD_NO].toString(),
                        avroCharge[Charge.ATTR_STATUS].toString(),
                        avroCharge[Charge.ATTR_AMOUNT] as Int,
                        avroCharge[Charge.ATTR_BRAND].toString()
                )
        )

        return chargeEvent
    }
}