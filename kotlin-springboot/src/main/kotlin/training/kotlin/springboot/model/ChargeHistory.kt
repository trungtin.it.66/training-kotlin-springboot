package training.kotlin.springboot.model

import java.math.BigInteger
import java.sql.Timestamp
import javax.persistence.*

@Entity
@Table(name = "CHARGE_HISTORY")
data class ChargeHistory (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val chargeHistoryId: BigInteger,
        val content: String,
        val createdTime: Timestamp
)