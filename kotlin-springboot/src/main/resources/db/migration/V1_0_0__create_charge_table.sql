CREATE TABLE IF NOT EXISTS CHARGE (
    CHARGE_ID INT GENERATED ALWAYS AS IDENTITY NOT NULL,
    CARD_NO VARCHAR (50) NOT NULL,
    STATUS VARCHAR (50) NOT NULL,
    AMOUNT INT NOT NULL,
    BRAND VARCHAR (50) NOT NULL
);